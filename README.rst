.. important::
  **THIS PROJECT IS NOW UNMAINTAINED.**

  We don't need this project anymore, so we don't develop it nor maintain it.

.. image:: http://unmaintained.tech/badge.svg
  :target: http://unmaintained.tech/
  :alt: No Maintenance Intended

Python TAP13 protocol parser
============================

Python parser for the Test Anything Protocol (TAP) version 13, as described in http://podwiki.hexten.net/TAP/TAP13.html?page=TAP13 .
