#!/usr/bin/env python
# Copyright 2019, Red Hat, Inc.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#   http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
# Author: Josef Skladanka <jskladan@redhat.com>

from distutils.core import setup

setup(  name = 'pytap13',
        py_modules = ['pytap13'],
        version = '0.3.0',
        description = 'Python parser for the Test Anything Protocol (TAP) version 13',
        author = 'Josef Skladanka',
        author_email = 'jskladan@redhat.com',
        url = "https://bitbucket.org/fedoraqa/pytap13",
        install_requires = ['yamlish'],
        classifiers = [

            "Development Status :: 2 - Pre-Alpha",
            "Intended Audience :: Developers",
            "License :: OSI Approved :: Apache Software License",
            "Operating System :: OS Independent",
            "Programming Language :: Python :: 2",
            "Topic :: Scientific/Engineering :: Interface Engine/Protocol Translator",
            "Topic :: Software Development :: Testing",
            "Topic :: Software Development :: Libraries :: Python Modules",
            ],

     )

