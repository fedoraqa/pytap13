# Copyright 2019, Red Hat, Inc.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#   http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
# Author: Josef Skladanka <jskladan@redhat.com>

.PHONY: test test-ci pylint pep8 docs clean virtualenv

# general variables
VENV=test_env
SRC=pytap13

# Variables used for packaging
SPECFILE=$(SRC).spec
BASEARCH:=$(shell uname -i)
DIST:=$(shell rpm --eval '%{dist}')
VERSION:=$(shell rpmspec -q --queryformat="%{VERSION}\n" $(SPECFILE) | uniq)
RELEASE:=$(subst $(DIST),,$(shell rpmspec -q --queryformat="%{RELEASE}\n" $(SPECFILE) | uniq))
NVR:=$(SRC)-$(VERSION)-$(RELEASE)
GITBRANCH:=$(shell git rev-parse --abbrev-ref HEAD)
TARGETDIST:=fc21
BUILDTARGET=fedora-21-x86_64

test: $(VENV)
	sh -c "TEST='true' . $(VENV)/bin/activate; py.test --cov $(SRC) testing/; deactivate"

test-ci: $(VENV)
	sh -c "TEST='true' . $(VENV)/bin/activate; py.test --cov-report xml --cov $(SRC) testing/; deactivate"

pylint:
	pylint -f parseable $(SRC) | tee pylint.out

pep8:
	pep8 $(SRC)/*.py $(SRC)/*/*.py | tee pep8.out

ci: test-ci pylint pep8

docs:
	sphinx-build  -b html -d docs/_build/doctrees docs/source docs/_build/html

clean:
	rm -rf dist
	rm -rf pytap13.egg-info
	rm -rf build
	rm -f pep8.out
	rm -f pylint.out

archive: $(SRC)-$(VERSION).tar.gz

$(SRC)-$(VERSION).tar.gz:
	git archive $(GITBRANCH) --prefix=$(SRC)-$(VERSION)/ | gzip -c9 > $@

mocksrpm: archive
	mock -r $(BUILDTARGET) --buildsrpm --spec $(SPECFILE) --sources .
	cp /var/lib/mock/$(BUILDTARGET)/result/$(NVR).$(TARGETDIST).src.rpm .

mockbuild: mocksrpm
	mock -r $(BUILDTARGET) --no-clean --rebuild $(NVR).$(TARGETDIST).src.rpm
	cp /var/lib/mock/$(BUILDTARGET)/result/$(NVR).$(TARGETDIST).noarch.rpm .

#kojibuild: mocksrpm
#	koji build --scratch dist-6E-epel-testing-candidate $(NVR).$(TARGETDIST).src.rpm

nvr:
	@echo $(NVR)

cleanvenv:
	rm -rf $(VENV)

virtualenv: $(VENV)

$(VENV):
	virtualenv --distribute --system-site-packages $(VENV)
	sh -c ". $(VENV)/bin/activate; pip install --force-reinstall -r requirements.txt; deactivate"

